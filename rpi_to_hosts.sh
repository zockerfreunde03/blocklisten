#! /bin/sh
if [ -z "$1" ]
then
	echo "rpi_to_hosts.sh INFILE OUTFILE"
	exit 1
elif [ -z "$2" ]
then
	echo "rpi_to_hosts.sh INFILE OUTFILE"
	exit 1
fi

infile="$1"
outfile="$2"

if ! [ -f "$infile" ]
then
	echo "$infile seems not to exist?"
	exit 1
fi

cat "$infile" | while read -r line
do
	case "$line" in 
		"#"*) true ;; # remove any comments
		*) printf "0.0.0.0\t%b\n" "$line" ;;
	esac
done > "$outfile"
