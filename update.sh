#! /bin/sh

BASE="$HOME/.config/blocklist"

prepare_base()
{
	if ! [ -e "$BASE" ] 
	then
		mkdir -p "$BASE"
	elif ! [ -d "$BASE" ]
	then
		rm "$BASE" && mkdir -p "$BASE"
	fi

	touch "$BASE/spam"
	touch "$BASE/malware"
	touch "$BASE/notserious"
	touch "$BASE/crypto"
	touch "$BASE/phishing"
	touch "$BASE/fake_science"
	touch "$BASE/w3kbl"
	touch "$BASE/easy-privacy"
	touch "$BASE/energized"
	touch "$BASE/ultimate"
}

merge()
{
	if ! diff "$1" "$1.new" > /dev/null
	then
		mv "$1.new" "$1"
	else
		rm "$1.new"
	fi
}

prepare_base

cwd=$(pwd)

cd "$BASE" || mkdir -p "$BASE" && cd "$BASE" || exit 1

id=$(timer.sh start)

echo "Spam Mails (1/10)"
curl -L "https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/spam.mails" > spam.new
echo "Malware (2/10)"
curl -L "https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/malware" > malware.new
echo "Fake Shops (3/10)"
curl -L "https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/notserious" > notserious.new
echo "Crypto Stuff (4/10)"
curl -L "https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/crypto" > crypto.new
echo "Phishing (5/10)"
curl -L "https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/Phishing-Angriffe" > phishing.new
echo "Fake Science (6/10)"
curl -L "https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/Fake-Science" > fake_science.new
echo "w3kbl.txt (7/10)"
curl -L "https://v.firebog.net/hosts/static/w3kbl.txt" > w3kbl.new
echo "Easy Privacy (8/10)"
curl -L "https://v.firebog.net/hosts/Easyprivacy.txt" > easy-privacy.new
echo "Energized Basic (9/10)"
curl -L "https://block.energized.pro/basic/formats/domains.txt" > energized.new
echo "Energized Ultimate (10/10)"
curl -L "https://block.energized.pro/ultimate/formats/domains.txt" > ultimate.new

merge spam
merge malware
merge notserious
merge crypto
merge phishing
merge fake_science
merge w3kbl
merge easy-privacy
merge energized
merge ultimate

echo "Combining, sorting and removing duplicates"
cat spam malware notserious crypto phishing fake_science w3kbl easy-privacy energized ultimate | sort -u | uniq > blockliste.domains

echo "Creating hosts file $HOSTS"
if command rpi2hosts --help 2> /dev/null
then
	rpi2hosts -f "$BASE/blockliste.domains" -o "$BASE/blockliste"
else
	echo "rpi2hosts not found, using shell script"
	echo "Warning: It is recommended to install rpi2hosts, since the shell script is quite slow"
	if ! [ -f "$BASE/rpi_to_hosts.sh" ]; then echo "ERROR: rpi_to_hosts.sh is not found in $BASE" && exit 1; fi
	/bin/sh "$BASE/rpi_to_hosts.sh" "$BASE/blockliste.domains" "$BASE/blockliste"
fi

timer.sh "$id"

rm blockliste.domains

cd "$cwd" || exit 1
