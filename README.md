# Hostfile blocklists

Curls Pi-Hole Lists from github.com/RPiList, merges them and then converts those
to Hosts file format.

If you want to get a report-diff then you need: ```git``` (for git-diff) and
a sendmail-like program in the path called ```sendmail``` that reads from stdin
when invoked with ```sendmail add``` and also a file called ```.report``` in
your $BASE dir
